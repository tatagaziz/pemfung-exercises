data Tree a = Leaf a | Branch (Tree a) (Tree a)

-- Define foldTree
foldTree :: (f -> a -> a) -> Tree a -> a

foldTree f (Leaf a) = f a