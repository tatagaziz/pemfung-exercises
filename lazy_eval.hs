-- Nomor 1
listNew :: [Integer]
listNew = [x+y | x <- [1..4], y <- [2..4], x>y]
-- >>> x
-- [5,6,7]
--

-- Urutan Evaluasi
-- x == 1, y == 2, karena x <= y, tidak menggenerasikan elemen 1 + 2
-- x == 1, y == 3, karena x <= y, tidak menggenerasikan elemen 1 + 3
-- x == 1, y == 4, karena x <= y, tidak menggenerasikan elemen 1 + 4

-- x == 2, y == 2, karena x <= y, tidak menggenerasikan elemen 2 + 2
-- x == 2, y == 3, karena x <= y, tidak menggenerasikan elemen 2 + 3
-- x == 2, y == 4, karena x <= y, tidak menggenerasikan elemen 2 + 4

-- x == 3, y == 2, karena x > y, menggenerasikan elemen pertama sebagai 3 + 2 == 5
-- x == 3, y == 3, karena x <= y, tidak menggenerasikan elemen 3 + 3
-- x == 3, y == 4, karena x <= y, tidak menggenerasikan elemen 3 + 4

-- x == 4, y == 2, karena x > y, menggenerasikan elemen kedua sebagai 4 + 2 == 6
-- x == 4, y == 3, karena x > y, menggenerasikan elemen ketiga sebagai 4 + 3 == 7
-- x == 4, y == 4, karena x <= y, tidak menggenerasikan elemen 4 + 4

-- Sehingga hasil list akhir == [5,6,7
--------------------------------------------------------------------------------------
-- Nomor 2

divisor :: Integer -> [Integer]
divisor a = [ x | x <- [1..a] , a `mod` x == 0]

-- >>> divisor 11
-- [1,11]
--

-- Nomor 3

quicksort :: Ord t => [t] -> [t]
quicksort [] = []
quicksort (x:xs) = quicksort([ls | ls <- xs, ls < x]) ++ 
                    [x] ++ 
                    quicksort([rs | rs <- xs, rs >= x])
-- >>> quicksort [78,123,33,2,124,123,8]
-- [2,8,33,78,123,123,124]
-- >>> :t quicksort
-- quicksort :: Ord t => [t] -> [t]
--

myFlip :: (t1 -> t2 -> t) -> (t2 -> t1 -> t)
myFlip f a b = f b a 

-- >>> myFlip (/) 12 10
-- 0.8333333333333334
--


